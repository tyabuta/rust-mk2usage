extern crate clap;

use clap::{App, Arg};

use mk2usage::hint::*;
use std::process::exit;


const ARG_NAME_ALL_COMMANDS: &str = "ALL_COMMANDS";
const ARG_NAME_ALL_VARIABLES: &str = "ALL_VARIABLES";
const ARG_NAME_MAKEFILE: &str = "MAKEFILE";


fn clap_app<'a>() -> clap::App<'a, 'a> {
    App::new("mk2usage")
        .version("1.0")
        .about("Prints a usage of the specified Makefiles.")
        .args(&[
            Arg::with_name(ARG_NAME_ALL_VARIABLES)
                .help("Shows all the variables")
                .short("V").long("all-variables"),
            Arg::with_name(ARG_NAME_ALL_COMMANDS)
                .help("Shows all the commands")
                .short("C").long("all-commands"),
            Arg::with_name(ARG_NAME_MAKEFILE)
                .help("Specified Makefiles")
                .multiple(true)
                .takes_value(true)
                .required(true),
        ])
}

fn main() {
    let matches = clap_app().get_matches();

    let show_all_variables = matches.is_present(ARG_NAME_ALL_VARIABLES);
    let show_all_commands = matches.is_present(ARG_NAME_ALL_COMMANDS);
    let files: Vec<&str> = matches.values_of(ARG_NAME_MAKEFILE).unwrap().collect();

    let parser = MakeFileParser::new(files);
    if let Err(e) = parser {
        eprintln!("failed to parse. cause:{}", e);
        exit(1);
    }

    parser.unwrap().output_with_setting(&OutputSetting {
        variable_shows_all: show_all_variables,
        command_shows_all: show_all_commands,
    });
}


#[cfg(test)]
mod test {
    use super::*;
    use std::fmt;

    macro_rules! impl_fmt {
        ($type:tt, $( $method:tt ),*) => {{
            impl fmt::Display for $type<'_> {
                fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {

                    let mut list: Vec<String> = Vec::new();
                    $(
                        list.push(format!("{}:{:?}", stringify!($method), self.$method));
                    )*
                    write!(f, "{}", list.join(", "))
                }
            }
        }};
    }

    #[test]
    fn test_cli_table() {
        struct Test<'a> {
            command_line: Vec<&'a str>,
            files: Vec<&'a str>,
            variables: bool,
            commands: bool,
        }
        impl_fmt!(Test, command_line);

        let table = [
            Test {
                command_line: vec!["mkusage", "hoge", "piyo"],
                files: vec!["hoge", "piyo"],
                variables: false,
                commands: false,
            },
            Test {
                command_line: vec!["mkusage", "-V", "hoge"],
                files: vec!["hoge"],
                variables: true,
                commands: false,
            },
            Test {
                command_line: vec!["mkusage", "-C", "hoge"],
                files: vec!["hoge"],
                variables: false,
                commands: true,
            },
            Test {
                command_line: vec!["mkusage", "-C", "-V", "hoge"],
                files: vec!["hoge"],
                variables: true,
                commands: true,
            },
            Test {
                command_line: vec!["mkusage", "--all-variables", "--all-commands", "hoge"],
                files: vec!["hoge"],
                variables: true,
                commands: true,
            },
        ];

        for t in table.iter() {
            let res = clap_app().get_matches_from_safe(&t.command_line);
            assert_eq!(res.is_ok(), true, "{}", t);

            let matches = res.unwrap();
            let files: Vec<&str> = matches.values_of(ARG_NAME_MAKEFILE).unwrap().collect();
            assert_eq!(files, t.files, "{}", t);

            assert_eq!(matches.is_present(ARG_NAME_ALL_VARIABLES), t.variables, "{}", t);
            assert_eq!(matches.is_present(ARG_NAME_ALL_COMMANDS), t.commands, "{}", t);
        }
    }
}

