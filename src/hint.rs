use regex::Regex;
use std::io::{Error, BufRead};
use std::io;
use std::path::Path;
use std::fs::File;


pub struct MakeFileParser {
    commands: Vec<Hint>,
    variables: Vec<Hint>,
}

pub struct OutputSetting {
    pub variable_shows_all: bool,
    pub command_shows_all: bool,
}


struct Hint {
    name: String,
    comments: Vec<String>,
}

fn output_hint(hint: &Hint, indent_space: &str, shows_all: bool) {
    if 0 == hint.comments.len() {
        if !shows_all {
            return;
        }

        println!("    {}", hint.name);
        return;
    }

    for (i, comment) in hint.comments.iter().enumerate() {
        if 0 == i {
            let name = format!("    {}", hint.name);
            let s = " ".repeat(indent_space.len() - name.len());
            println!("{}{} -- {}", name, s, comment);
            continue;
        }

        println!("{}    {}", indent_space, comment)
    }
}


impl MakeFileParser {
    pub fn new(file_names: Vec<&str>) -> Result<MakeFileParser, Error> {
        let mut variable_hints = Vec::new();
        let mut command_hints = Vec::new();

        for f in &file_names {
            let (v_hints, c_hints) = read_command_hints(f)?;
            variable_hints.extend(v_hints);
            command_hints.extend(c_hints);
        }

        Ok(MakeFileParser {
            commands: command_hints,
            variables: variable_hints,
        })
    }

    pub fn output(&self) {
        self.output_with_setting(&OutputSetting {
            variable_shows_all: false,
            command_shows_all: false,
        });
    }

    pub fn output_with_setting(&self, setting: &OutputSetting) {
        let indent_space_count = self.variables.iter()
            .chain(self.commands.iter())
            .map(|o| o.name.len())
            .max()
            .unwrap_or_default();

        let indent_space = " ".repeat(4 + indent_space_count + 1);


        println!("USAGE");
        println!("    make <COMMANDS> [VARIABLES]");
        println!();

        if 0 < self.variables.len() {
            println!("VARIABLES");
            for v in &self.variables {
                output_hint(v, &indent_space, setting.variable_shows_all);
            }
            println!();
        }

        if 0 < self.commands.len() {
            println!("COMMANDS");
            for v in &self.commands {
                output_hint(v, &indent_space, setting.command_shows_all);
            }
            println!();
        }
    }
}


fn read_command_hints(file_name: &str) -> Result<(Vec<Hint>, Vec<Hint>), Error> {
    let mut variables_hints = Vec::new();
    let mut command_hints = Vec::new();
    let mut buf = Vec::new();

    let iterator = read_lines(file_name)?;
    for line_result in iterator {
        let line = line_result?;

        if line.starts_with("## ") {
            buf.push(line[3..].to_string());
            continue;
        }

        if let Some(name) = find_variable_name(&line) {
            variables_hints.push(Hint { name, comments: buf.to_vec() });
            buf.clear();
            continue;
        }

        if let Some(name) = find_command_name(&line) {
            command_hints.push(Hint { name, comments: buf.to_vec() });
            buf.clear();
            continue;
        }

        if 0 < buf.len() {
            buf.clear();
        }
    }

    Result::Ok((variables_hints, command_hints))
}


fn find_command_name(s: &str) -> Option<String> {
    let re = Regex::new(r"^(\w[\w-]*):.*$").unwrap();
    let text = re.captures(s)?.get(1)?.as_str();
    Option::Some(text.to_string())
}

fn find_variable_name(s: &str) -> Option<String> {
    let re = Regex::new(r"^(\w+)\s*(=|\?=|:=|::=|\+=|!=).*$").unwrap();
    let text = re.captures(s)?.get(1)?.as_str();
    Option::Some(text.to_string())
}


fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


#[cfg(test)]
mod tests {
    use super::*;
    use std::string::ToString;

    #[test]
    fn test_make_file_parser_output() {
        let parser = MakeFileParser::new(vec!["tests/sample1.mk"]);
        assert_eq!(parser.is_ok(), true);

        parser.unwrap().output();
    }


    #[test]
    fn test_make_file_parser() {
        let parser = MakeFileParser::new(vec!["tests/sample1.mk"]);
        assert_eq!(parser.is_ok(), true);

        let parser = parser.unwrap();
        assert_eq!(parser.commands.len(), 13);
        assert_eq!(parser.variables.len(), 5);

        let parser = MakeFileParser::new(vec!["tests/sample1_.mk"]);
        assert_eq!(parser.is_ok(), false);
    }


    #[test]
    fn test_find_command_name() {
        let ret = find_command_name("Sample1: sample1.cpp").unwrap_or("".to_string());
        assert_eq!(ret, "Sample1");

        let ret = find_command_name("Sample2 sample1.cpp").unwrap_or("".to_string());
        assert_eq!(ret, "");
    }

    #[test]
    fn test_find_variable_name() {
        let ret = find_variable_name("PWD3:=$(shell pwd)").unwrap_or("".to_string());
        assert_eq!(ret, "PWD3");

        let ret = find_variable_name("Sample1: sample1.cpp").unwrap_or("".to_string());
        assert_eq!(ret, "");
    }
}
