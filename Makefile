
test:
	cargo test

run-help:
	cargo run -- --help

build:
	cargo build --release
	ls -lha target/release

build-for-linux:
	cross build --release --target x86_64-unknown-linux-gnu

build-for-win:
	cross build --release --target x86_64-pc-windows-gnu

