

## (prod|dev1)
PWD=$(shell pwd)
PWD2?=$(shell pwd)

PWD3:=$(shell pwd)

task2:
	@echo task2

## task3 desu
## multi line
task3:
	@echo task3

## task1 desu
task1:
	@echo task1


CC = g++

## CFLAGS
CFLAGS = -g -Wall

Sample1: sample1.cpp
	$(CC) $(CFLAGS) -o sample1 sample1.cpp

Sample2: sample2.cpp
	$(CC) $(CFLAGS) -o sample2 sample2.cpp


## GOAL
.DEFAULT_GOAL := help

## Run tests
test: deps
	go test ./...

## Install dependencies
deps:
	go get -d -v -t ./...


## Lint
lint: dev-deps
	go vet ./...
	golint -set_exit_status ./...

## Take coverage
cover: dev-deps
	goveralls

bin/%: cmd/%/main.go
	go build -ldflags "$(LDFLAGS)" -o $@ $<

## Release the binaries
release:
	_tools/releng


## Show log with `less`
log-less:
	echo log

## Show log with `tail`
log-tail:
	echo log


## Show help
help:
	@make2help $(MAKEFILE_LIST)

.PHONY: test dev-deps deps lint cover release help

